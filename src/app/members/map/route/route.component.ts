import { Component, OnInit, Input, HostListener } from '@angular/core';
import { ModalController, LoadingController} from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { Constants } from 'src/app/constants.enum';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from './../../../services/authentication.service';
import { createNgModule } from '@angular/compiler/src/core';
import { DeviceMarkers } from 'src/app/geo-json-template';
import { Keyboard } from '@ionic-native/keyboard/ngx';


@Component({
  selector: 'app-route',
  templateUrl: './route.component.html',
  styleUrls: ['./route.component.scss'],
})
export class RouteComponent implements OnInit {

  isDeviceList = false;
  @Input() data;
  deviceMarkers: DeviceMarkers[] = [];

  userId: number;
  url: string = Constants.ROUTE;
  deviceAllData;
  deviceData;
  selDeviceID;
  startDate;
  endDate;
  lastMinutes;
  lastPoints;
  selectDays;

  month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];


  constructor(
    public loadingCtrl: LoadingController,
    private modalController: ModalController,
    private storage: Storage,
    private authService: AuthenticationService,
    private keyboard: Keyboard
  ) {}

  async ngOnInit() {
    await this.storage.get(Constants.USER_ID).then((res) => {
      this.userId = res;
    });
    this.loadData();
  }

  async loadData() {
    await this.storage.get('selected-device-data').then(res => {
      this.enableDeviceData(res.deviceID);
      const index = this.data.findIndex(obj =>
        obj.id === res.deviceID
      );
      this.deviceAllData = this.data[index];
    });
  }

  dismiss(type: string = '') {
    this.modalController.dismiss({
      type,
      dismissed: true
    });
  }

  async hideDevices() {
    this.isDeviceList = false;
    this.loadData();
  }

  enableDeviceData(deviceID) {
    const index = this.data.findIndex(obj =>
      obj.id === deviceID
      );
    const startDate = new Date((this.data[index].spurdatum)  * 1000);
    const endDate = new Date((this.data[index].spurdatumbis)  * 1000);
    this.startDate = this.convertDateUnixtoTime(startDate);
    this.endDate = this.convertDateUnixtoTime(endDate);
    this.lastMinutes = this.data[index].spurminuten;
    this.lastPoints = this.data[index].spurpunkte;
  }

  convertDateUnixtoTime(dateunix) {
    const allMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const minutes =  '0' + dateunix.getMinutes();
    const convdataTime = dateunix.getDate() + ' ' + allMonths[dateunix.getMonth()] + ' ' + dateunix.getFullYear() + ', ' +
                                dateunix.getHours() + ':' + minutes.substr(-2);
    return convdataTime;
  }

  async getPoints(form: NgForm) {
    const loading = await this.loadingCtrl.create({
      message: 'Please Wait',
      translucent: true,
    });
    await loading.present();

    const startDate = Date.parse(this.startDate);
    const endDate = Date.parse(this.endDate);

    await this.storage.get('selected-device-data').then((res) => {
      this.selDeviceID = res.deviceID;
    });
    await this.authService
      .updateSpurInfo(
        this.selDeviceID,
        3,
        startDate / 1000,
        endDate / 1000,
        +this.lastMinutes,
        +this.lastPoints
      )
      .then((res) => {});
    this.dismiss('updateRoute');
    loading.dismiss();
  }

  optionDateSelector(selected) {

    var date = new Date();
    var today = new Date();


    switch (selected) {
      case 'yesterday':

        date.setDate(date.getDate() - 1);
        // var formateDate = date.toLocaleString();
        // MMM DD, YYYY HH:mm
        this.startDate = this.month[date.getMonth()]
                          + ' ' + date.getDate() + ', '
                          + date.getFullYear()
                          + ' ' + date.getHours()
                          + ':' + date.getMinutes();
        this.endDate = this.startDate;

        break;
      case 'today':

        // var formateDate = date.toLocaleString();
        this.startDate = this.month[date.getMonth()]
                          + ' ' + date.getDate() + ', '
                          + date.getFullYear()
                          + ' ' + date.getHours()
                          + ':' + date.getMinutes();
        this.endDate = this.startDate;

        break;
      case 'last-30-days':

       // var todayFormateDate = today.toLocaleString();
        this.endDate = this.month[today.getMonth()]
                      + ' ' + today.getDate() + ', '
                      + today.getFullYear()
                      + ' ' + today.getHours()
                      + ':' + today.getMinutes();

        date.setDate(date.getDate() - 30);
        // var formateDate = date.toLocaleString();
        this.startDate = this.month[date.getMonth()]
                        + ' ' + date.getDate() + ', '
                        + date.getFullYear()
                        + ' ' + date.getHours()
                        + ':' + date.getMinutes();

        break;
      default:
        break;
    }
    console.log( 'this.startDate', this.startDate, '\n this.endDate', this.endDate);
  }

  logScrolling(event) {
    this.keyboard.hide();
  }
}
